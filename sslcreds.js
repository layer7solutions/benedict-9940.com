const fs = require('fs');

module.exports = {
	key: fs.readFileSync(process.env.SSL_KEY, 'utf8'),
	cert: fs.readFileSync(process.env.SSL_CERT, 'utf8'),
	ca: fs.readFileSync(process.env.SSL_CA, 'utf8'),
};