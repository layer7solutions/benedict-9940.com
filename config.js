// THIS CONFIG IS NOT FOR SECRETS
// DO NOT PUT ANY SECRETS OR PRIVATE KEYS IN THIS FILE

// The .env file should be used for secrets. You can access .env
// properties  using `process.env.PROP_NAME`

const path = require('path');
const session = require('express-session');
const pgSession = require('connect-pg-simple')(session);
const DatabasePool = require('@db').getPool();

module.exports = {
  auth: {
    discordOAuth: {
      clientID: process.env.OAUTH_DISCORD_CLIENT_ID,
      clientSecret: process.env.OAUTH_DISCORD_CLIENT_SECRET,
      callbackURL: process.env.OAUTH_DISCORD_REDIRECT_URI,
      scope: process.env.OAUTH_DISCORD_SCOPES.split(/ |,/g).filter(x => x),
    },
    paths: {
      login: '/auth/discord',
      loginCallback: process.env.OAUTH_DISCORD_REDIRECT_PATH,
      logout: '/auth/logout',
      authInfo: '/auth/info',
      loginInterstitial: '/auth/interstitial',
    },
    postAuthRedirects: {
      // Redirect uri if authentication fails
      failureRedirect: '/auth/discord/failed',

      // If req.session.returnTo exists, the user will be redirect to that otherwise
      // redirected to the value here.
      successReturnToOrRedirect: '/'
    },
    successRedirectDelay: 2000,
  },
  session: session({
    store: new pgSession({
      pool: DatabasePool,
      tableName: 'websessions',
    }),
    secret: process.env.SESSID_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 30 * 24 * 60 * 60 * 1000, // 30 days
      httpOnly: true,
      secure: true,
    },
  }),
  views: {
    root: path.resolve(__dirname, './views'),
    publicDir: path.resolve(__dirname, './public'),
    siteTitle: 'Benedict-9940',
    base: {
      layouts: ['layouts/base-layout'],
      styles: ['style'],
      scripts: [],
      bodyClass: [],
    },
    ejsDelimiter: '%',
    formatPageTitle:
      (siteTitle, pageTitle) => pageTitle ? `${pageTitle} | ${siteTitle}` : siteTitle,
  },
  services: {
    modmail: {
      name: 'modmail',
      icon: 'mail',
    },
    infractions: {
      name: 'infractions',
      icon: 'alert-triangle',
    },
    messages: {
      name: 'messages',
      icon: 'message-circle',
    },
    serversettings: {
      name: 'serversettings',
      icon: 'sliders',
    },
    tags: {
      name: 'tags',
      icon: 'tag',
    },
    blacklist: {
      name: 'blacklist',
      icon: 'slash',
    }
  }
};
