require('module-alias/register');
require('dotenv').config();

const express = require('express');
const http = require('http');
const https = require('https');
const vhost = require('vhost');
const app = require('@/src/app.js');
const sslcreds = require('@/sslcreds.js');

const server = express();
server.use(vhost(process.env.VHOST, app));

http.createServer(server).listen(80, () => {
  console.log('HTTP Server running on port 80');
});

https.createServer(sslcreds, server).listen(443, () => {
  console.log('HTTPS Server running on port 443');
});

module.exports = server;
