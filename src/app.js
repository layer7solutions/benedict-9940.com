const config = require('@/config.js')
const express = require('express');
const compression = require('compression');
const helmet = require('helmet');
const path = require('path');

const ejs = require('ejs');
ejs.delimiter = config.views.ejsDelimiter;
ejs.root = config.views.root;

const app = express();
app.set('views', config.views.root);
app.set('view engine', 'ejs');
app.use(compression());
app.use(helmet());
app.use(express.urlencoded({extended: true}));
app.use(express.static(config.views.publicDir));

app.use(require('@/src/middleware/headers.js'));
app.use(require('@/src/middleware/sessions.js'));
app.use('/', require('@/src/controllers'));

app.get('*', function(req, res) {
  // 404-Handler: this must always be last.
  res.status(404).render('pages/errorPages/404');
});

module.exports = app;