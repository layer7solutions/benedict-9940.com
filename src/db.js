const { Pool } = require('pg');
const pool = new Pool(); // uses settings from .env to connect to database

module.exports = {
  lastQuery: {
    executionTime: undefined,
    rowCount: undefined,
    fields: undefined,
    command: undefined,
  },

  getPool() {
    return pool;
  },

  async rawQuery(config) {
    const startTime = Date.now();
    const { rows, rowCount, fields, command } = await pool.query(config);
    this.lastQuery.executionTime = Date.now() - startTime;
    this.lastQuery.rowCount = rowCount;
    this.lastQuery.fields = fields;
    this.lastQuery.command = command;
    return rows;
  },

  async query(text, params) {
    return await this.rawQuery({text, values: params});
  },

  async queryList(text, params) {
    return await this.rawQuery({text, values: params, rowMode: 'array'});
  },

  async queryFirstRow(text, params) {
    const rows = await this.query(text, params);
    return rows.length ? rows[0] : undefined;
  },

  async queryFirstList(text, params) {
    const rows = await this.queryList(text, params);
    return rows.length ? rows[0] : undefined;
  },

  async queryFirstColumn(text, params) {
    const rows = await this.queryList(text, params);
    return rows.map(row => row[0]);
  },

  async queryOneColumn(column, text, params) {
    if (typeof column === 'number') {
      const rows = await this.queryList(text, params);
      return rows.map(row => row[column]);
    } else {
      const rows = await this.query(text, params);
      return rows.map(row => row[column]);
    }
  },

  async queryFirstField(text, params) {
    const firstRow = await this.queryFirstList(text, params);
    return firstRow && firstRow[0];
  },

  async queryOneField(field, text, params) {
    if (typeof field === 'number') {
      const firstRow = await this.queryFirstList(text, params);
      return firstRow && firstRow[field];
    } else {
      const firstRow = await this.queryFirstRow(text, params);
      return firstRow && firstRow[field];
    }
  },
};
