const router = require('@router').create();

const db = require('@db');
const config = require('@/config.js');
const models = require('@/src/models');

router.get('/', async (req, res) => {
  if (req.isAuthenticated()) {
    const firstAvailableServer = await models.servers.getFirstServer();
    res.redirect('/s-' + firstAvailableServer.id);
  } else {
    res.render('pages/homepage', {
      styles: ['pages/homepage']
    });
  }
});

router.get('/homepage', async (req, res) => {
  res.render('pages/homepage', {
    styles: ['pages/homepage']
  });
});

module.exports = router;
