const db = require('@db');
const config = require('@/config.js');
const models = require('@/src/models');

const router = require('@router').create();

router.get('/s-:serverId', async (req, res) => {
  const serverId = req.params.serverId;

  res.render('pages/servicelist', {
    serverId: serverId,
    servers: await models.servers.getServers(),
    services: Object.values(config.services),
  });
});

module.exports = router;
