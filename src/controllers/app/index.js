const router = require('@router').create({
  layouts: ['layouts/app-layout'],
  bodyClass: ['in-app'],
  styles: ['application'],
});

router.use(require('./ServiceList.js'));

module.exports = router;
