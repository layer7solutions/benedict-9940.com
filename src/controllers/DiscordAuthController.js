const router = require('@router').create();

const passport = require('passport');
const config = require('@/config.js');
const checkauth = require('@checkauth');
const ejs = require('ejs');

router.get(config.auth.paths.login,
  checkauth.setReturnTo(),
  passport.authenticate('discord', { scope: config.auth.discordOAuth.scope })
);

router.get(config.auth.paths.loginCallback,
  passport.authenticate('discord', {
    failureRedirect: config.auth.postAuthRedirects.failureRedirect,
  }),
  function(req, res) {
    res.redirect(
      config.auth.paths.loginInterstitial +
        '?cont=' +
        checkauth.getReturnTo(req, config.auth.postAuthRedirects.successReturnToOrRedirect)
    );
  }
);

router.get(config.auth.paths.logout, function(req, res) {
  req.logout();
  res.redirect(config.auth.paths.loginInterstitial + '?cont=' + (req.query.cont || '/'));
});

router.get(config.auth.paths.loginInterstitial, function(req, res) {
  const cont = req.query.cont || '/';

  const html = `<!doctype html>
  <html><body>
    <script>window.location.href = "<%= cont %>";</script>
  </body></html>`;

  res.set('Content-Type', 'text/html');
  res.send(Buffer.from(ejs.render(html, { cont })));
});

router.get(config.auth.paths.authInfo, checkauth.authRequired(), (req, res) => {
  res.json(req.user);
});

module.exports = router;
