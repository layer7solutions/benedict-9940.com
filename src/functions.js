
// PHP trim() equivalent
function trim(str, char_mask, mode) {
    if (typeof str !== 'string') {
        str += '';
    }

    var l = str.length,
        i = 0;

    if (!l) return '';

    if (char_mask) {
        char_mask = char_mask+'';
        if (!char_mask.length) return str;
    } else {
        char_mask = trim.whitespace;
    }

    mode = mode || (1 | 2);

    // noinspection JSBitwiseOperatorUsage
    if (mode & 1) {
        for (i = 0; i < l; i++) {
            if (char_mask.indexOf(str.charAt(i)) === -1) {
                str = str.substring(i);
                break;
            }
        }
        if (i == l) return '';
    }

    // noinspection JSBitwiseOperatorUsage
    if (mode & 2) {
        for (i = l - 1; i >= 0; i--) {
            if (char_mask.indexOf(str.charAt(i)) === -1) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        if (i == -1) return '';
    }

    return str;
}
trim.whitespace = [
    ' ',
    '\n',
    '\r',
    '\t',
    '\f',
    '\x0b',
    '\xa0',
    '\u2000',
    '\u2001',
    '\u2002',
    '\u2003',
    '\u2004',
    '\u2005',
    '\u2006',
    '\u2007',
    '\u2008',
    '\u2009',
    '\u200a',
    '\u200b',
    '\u2028',
    '\u2029',
    '\u3000'
].join('');

function ltrim(str, char_mask) {
    return trim(str, char_mask, 1);
}
function rtrim(str, char_mask) {
    return trim(str, char_mask, 2);
}

module.exports = {
  trim,
  ltrim,
  rtrim,
}