const fs = require('fs');
const ejs = require('ejs');
const path = require('path');
const config = require('@/config.js');
const feather = require('feather-icons');
const functions = require('@/src/functions.js');

function resolveViewPath(view) {
  return path.resolve(config.views.root, functions.ltrim(view, '/\\') + '.ejs');
}

function icon(iconName, props = {}) {
  props.class = props.class ? (props.class = 'icon ' + props.class) : 'icon';
  return feather.icons[iconName].toSvg(props);
}

function addPageLocals(reqLocals, locals, view) {
  const pageLocals = {
    siteTitle: config.views.siteTitle,
    title: config.views.formatPageTitle(config.views.siteTitle, locals.title),
    styles: reqLocals.styles,
    scripts: reqLocals.scripts,
    bodyClassArray: reqLocals.bodyClass,
    bodyClass: reqLocals.bodyClass.join(' '),
  };

  const rootLocals = {
    page: pageLocals,
    subview: undefined,
    sublocals: {},
  };

  let pointer = rootLocals;
  let layouts = reqLocals.layouts.slice(1).concat(view);

  while (layouts.length) {
    pointer.subview = layouts[0];
    pointer.sublocals = { page: pageLocals };
    pointer = pointer.sublocals;
    layouts = layouts.slice(1);
  }

  Object.assign(pointer, locals);

  return rootLocals;
}

function createIncludeFunction(req) {
  return function include(view, locals) {
    return ejs.render(
      fs.readFileSync(resolveViewPath(view), 'utf8'),
      Object.assign(locals, {
        include,
        icon,
        loggedIn: req.isAuthenticated(),
        user: req.user || {},
        authLinks: config.auth.paths,
      }),
      {
        delimiter: config.views.ejsDelimiter,
      }
    );
  };
}

function updateReqLocals(req, options, deleteFromOptions = false) {
  if (!req.locals) req.locals = {};
  if (!req.locals.layouts) req.locals.layouts = config.views.base.layouts;
  if (!req.locals.styles) req.locals.styles = config.views.base.styles;
  if (!req.locals.scripts) req.locals.scripts = config.views.base.scripts;
  if (!req.locals.bodyClass) req.locals.bodyClass = config.views.base.bodyClass;

  if (options.layouts && options.layouts.length)
    req.locals.layouts = req.locals.layouts.concat(options.layouts);
  if (options.styles && options.styles.length)
    req.locals.styles = req.locals.styles.concat(options.styles);
  if (options.scripts && options.scripts.length)
    req.locals.scripts = req.locals.scripts.concat(options.scripts);
  if (options.bodyClass && options.bodyClass.length)
    req.locals.bodyClass = req.locals.bodyClass.concat(options.bodyClass);

  if (deleteFromOptions) {
    delete options.layouts;
    delete options.styles;
    delete options.scripts;
    delete options.bodyClass;
  }
}

module.exports =  {
  create(options = {layouts: [], styles: [], scripts: [], bodyClass: []}) {
    const router = require('express').Router();

    router.use(function(req, res, next) {
      updateReqLocals(req, options);

      res.render = function(view, locals, callback) {
        const include = createIncludeFunction(req);

        updateReqLocals(req, locals, true);

        const rendered = include(req.locals.layouts[0], addPageLocals(req.locals, locals, view));

        res.set('Content-Type', 'text/html');
        res.send(rendered);
      };

      next();
    });

    return router;
  }
};