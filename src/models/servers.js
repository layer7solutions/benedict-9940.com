const db = require('@db');

module.exports = {
  async getFirstServer() {
    return await db.queryFirstRow('SELECT * FROM server LIMIT 1');
  },
  async getServers() {
    return await db.query('SELECT * FROM server');
  },
};