const passport = require('passport');
const DiscordStrategy = require('passport-discord').Strategy;
const config = require('@/config.js');

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

passport.use(
  new DiscordStrategy(config.auth.discordOAuth, function(accessToken, refreshToken, profile, done) {
    process.nextTick(() => done(null, profile));
  })
);

module.exports = [
  config.session,
  passport.initialize(),
  passport.session(),
];