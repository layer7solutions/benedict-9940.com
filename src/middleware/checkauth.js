module.exports = {
  authRequired() {
    return function checkAuth(req, res, next) {
      if (req.isAuthenticated()) return next();
      res.render('pages/errorPages/loginRequired');
    };
  },
  setReturnTo() {
    return function(req, res, next) {
      if (req.query.cont)
        req.session.returnTo = req.query.cont;
      next();
    };
  },
  getReturnTo(req, defaultURL) {
    if (req.session.returnTo) {
      let cont = req.session.returnTo;
      delete req.session.returnTo;
      return cont;
    } else if (req.query.cont) {
      return req.query.cont;
    } else {
      return defaultURL;
    }
  },
};