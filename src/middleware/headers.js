module.exports = function(req, res, next) {
  res.header('X-Content-Type-Options', 'nosniff');
  res.header('X-XSS-Protection', '1; mode=block');
  res.header('X-Frame-Options', 'SAMEORIGIN');
  res.header('X-Clacks-Overhead', 'GNU Terry Pratchett');
  res.header('Referrer-Policy', 'same-origin');
  if (req.headers.host === process.env.VHOST) {
    res.header('Strict-Transport-Security', 'max-age=31536000; includeSubDomains');
  }
  next();
};
